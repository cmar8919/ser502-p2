import re

class Scanner(object):
    def __init__(self, prog):
        super(Scanner, self).__init__()
        self._lines = prog
        self._tokens = []
        self._current_token = ""
        self._line_number = 1
        
        # Build regular expression for tokenizing
        self._token_regex = "[a-zA-Z][a-zA-Z0-9_]*"     # Identifier
        self._token_regex += "|[0-9]+"                  # Integer
        self._token_regex += "|" + re.escape("==")
        self._token_regex += "|" + re.escape("!=")
        self._token_regex += "|" + re.escape(">=")
        self._token_regex += "|" + re.escape("<=")
        self._token_regex += "|[" + re.escape("=+-*/><!,&|") + "]" #adding '\' requires '\\' to escape it
        self._token_regex += "|[" + re.escape("(){}") + "]"
        self._token_regex += "|[\S]"    # Any non-whitespace character
        self._token_regex = re.compile(self._token_regex)   #compile into regex object
    
        self.next_line()    # Tokenizes first line
        
        
    """
    Tokenizes next line (ignoring empty lines)
    returns the first token in the new line if available
    returns None if there are not more lines
    Convention: place at the end of a successfully parsed line
    """
    def next_line(self):
        #print ""    # Prints new line in program echo
        if not self._lines:
            self._tokens = []
            self._current_token = ""
            return None

        while not self._lines[0].lstrip():  # lstrip() removes preceeding whitespace
            self._lines = self._lines[1:]   # remove first line in list if empty
            self._line_number += 1          # increment line counter
        self._tokens = self._token_regex.findall(self._lines[0])    # tokenize first line in list
        self._lines = self._lines[1:]       # remove first line in list
        self._line_number += 1              # increment line counter
        #print "  #Tokenized to: " + str(self._tokens)
        self._current_token = self._tokens[0]
        self._tokens = self._tokens[1:]     #remove first token in list
        return self._current_token
    
    """
    Gets next token and updates current token
    """
    def scan(self):
        #print self._current_token,
        if(not self._tokens):     #check for empty string
            return None
        self._current_token = self._tokens[0]
        self._tokens = self._tokens[1:] #remove first token in list
        return self._current_token
    
    """
    Returns current token
    """    
    def current(self):
        return self._current_token

    """
    Returns True if line is empty, otherwise False
    """
    def empty(self):
        if(not self._tokens):     # Check if line of tokens is empty
            return True
        return False
    
    """
    Returns True if the end of the program has been reached, otherwise False
    """    
    def end(self):
        if(not self._lines and not self._tokens):
            return True
        return False
        
    def get_line_number(self):
        return self._line_number
        
    def disp_token_error(self, function_name):
        print "\n    ****unexptected token: " + self._current_token + " at line: " + str(self._line_number) + " while looking for " + function_name
