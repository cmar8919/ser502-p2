import re
import sys

from cydwinder_scanner import Scanner
from cydwinder_symbol_table import Symbol_Table
gen_line = 0

'''
Get input file name to run as input argument
'''
if len(sys.argv) != 2:
    print "\n  ERROR: compiler expects a single file name\n"
    sys.exit()
    
file_name = sys.argv[1]

try: 
    program_file = open(file_name, 'r')
except IOError:
    print "\n  ERROR:  " + file_name + "  not in the local\n"
    sys.exit()


#Set file name
#file_name = "program.txt"
#file_name = "MathTest.txt"
#file_name = "MathTest2.cyd"
#file_name = "LoopTest.cyd"
#file_name = "FunctionTest.cyd"
#file_name = "IfTest.cyd"
#file_name = "Fibonacci.cyd"
#file_name = "Factorial.cyd"


# Create intermediate code output file name
int_file = file_name + ".int"

cyd_keywords = ["if", "else", "while", "int", "boolean", "return", "print", "get", "True", "False"]
cyd_types = ["int", "boolean"]
logical_op = ["<", "<=", ">", ">=", "==", "!="]
bool_op = ["&", "|", "!", "==", "!="]
instruction_set =[]

opcode_dict = {'<':'slt', '<=':'slte', '>':'sgt', '>=':'sgte', '==':'equ', '!=':'neq'}

t = Symbol_Table()

#Recursive Descent Parser
def cydwinder_compiler(prog):
    sc = Scanner(prog)
    if(not program(sc)):
        print "\n   Parse error at line: " + str(sc.get_line_number())
        return False
    output_file = open(int_file, 'w')
    for x in instruction_set:
        output_file.write(x + "\n")
    output_file.close()
    return True
            

def program(sc):
    while(not sc.end()):
        if loop(sc):
            sc.next_line()
        elif function(sc):
            sc.next_line()
        elif program_line(sc):
            sc.next_line()
        else:
            return False
    instruction_set.append("exit")
    return True


def program_line(sc):
    if assignment(sc):
        return True
    elif declaration(sc):
        return True
    elif if_stat(sc):
        return True
    elif builtin_function(sc):
        return True
    else:
        return False

    
def if_stat(sc):
    """
    if(logic)
        block
    else
        block
    
    else is optional, but must immediately follow IF block
    """
    #If statement
    if(sc.current() != "if"):
        return False
    # start of boolean check
    if(sc.scan() != "("):
        return False
    # check for variable identifier
    sc.scan()
    check, ident = identifier(sc)
    if(not check):
        return False
    if(sc.current() != ")"):
        return False
    if(not end_check(sc)):
        return False
    start_line = len(instruction_set)
    instruction_set.append("if_stat Start")    #place holder for debug
    sc.next_line()
    if(not block(sc)):
        return False
    sc.next_line()
    
    # Check if there is an else
    if(sc.current() != "else"):
        end_line = len(instruction_set) + 3
        instruction_set[start_line] = "bne " + ident + " True " + str(end_line)
        program_line(sc)     # Fix for getting next line even if it is not an ELSE
        return True
    
    # Handle else statment
    sc.scan() #remove else
#    print "  ** found an else"
    if not sc.empty():
        sc.disp_token_error("end of line after ELSE statement")
        return False
    sc.next_line()
    end_line = len(instruction_set) + 2
    instruction_set[start_line] = "bne " + ident + " True " + str(end_line)
    start_line_2 = len(instruction_set)
    instruction_set.append("Skip Else block here") #place holder for debug
#    start_line_2 = len(instruction_set)
    if(not block(sc)):
        return False
    end_line_2 = len(instruction_set) + 1
    instruction_set[start_line_2] = "jmp " + str(end_line_2)
    return True
     
        
def builtin_function(sc):
    if(sc.current() == "print"):
        sc.scan()
        check, ident = identifier(sc)
        if(check and sc.empty()):
            instruction_set.append("print " + ident)
            return True
    elif(sc.current() == "get"):
        sc.scan()
        check, ident = identifier(sc)
        if(check and sc.empty()):
            instruction_set.append("get " + ident)
            return True
    return False
    

"""
Define a function and compile it's block
"""
def function(sc):
    # keyword def
    if(sc.current() != "def"):
        return False
    returnType = sc.scan()
    if(not primative(sc)):
        sc.disp_token_error("function return type")
        return False
    # Check for fucntion name identifier
    check, name = identifier(sc)
    if(check == False):
        return False
    if(name in cyd_keywords):
        return False
    # Add function name to symbol table
    if(t.findSymbol(name)):
        typeError(sc, "variable or function with this name already exists")
        return False
    check,args_list,count = args(sc)
    if(not check):
        return False
    sc.next_line()
    gen_line = len(instruction_set)
    # Add function name to symbol table
    symbol_entry = (gen_line,returnType,count)
    t.enterSymbol(name, symbol_entry)
    start_line = len(instruction_set)
    instruction_set.append("Function Start")
    for x in range(0,len(args_list)):
        instruction_set.append(args_list[x])
    if(not functionBlock(sc)):
        return False
    end_line = len(instruction_set) + 1    # Line after jump to start of loop
    instruction_set[start_line] = "jmp " + str(end_line)
    return True


def args(sc):
    args_list = []
    if(sc.current() != "("):
        return False,None,None
    token = sc.scan()
    count = 0
    while(token != ")"):
        check,dataType = primative(sc)
        if not check :
            return False,None,None
        #intCode += value+" "
        check,name = identifier(sc)
        token = sc.current()
        if not check:
            return False,None,None
        #intCode += value
        if (t.enterSymbol(name, dataType) != dataType):
            typeError(sc, "variable name has already been declared with a different type")
#        args_list.append("pop "+name)
        # Using new intstruction to force top level scope
        args_list.append("arg " + name)
        count += 1
        if( token == ","):
            count += 1
            token = sc.scan()   # Remove ','
    if(not end_check(sc)):
        return False,None,None
    return True,args_list,count


def functionBlock(sc):
    token = sc.current()
    if(token != "{"):
        return False
    if(not end_check(sc)):
        return False
    t.enterScope()
    sc.next_line()
    # changed to check "return" stmt, then check for }
    while(sc.current() != "return" ):
        if(not program_line(sc)):
            return False
        if(sc.next_line() == None):
            return False
    sc.scan()
    if not identifier(sc):
        return False
    if not t.findSymbol(sc.current()):
        return False
    returnValue = sc.current()
    sc.next_line()
    if(not end_check(sc)):
        return False
    if sc.current() != "}":
        return False
    # return instruction for return value
    instr = "return "+returnValue
    # generate intermediate code based on returned values
    instructions = t.exitScope()
    # Restore previous values before returning
#    for x in range(0, len(instructions)):
#        instruction_set.append(instructions[x])
    instruction_set.append(instr)
    return True


def loop(sc):
    # keyword while
    if(sc.current() != "while"):
        return False
    # start of boolean check
    if(sc.scan() != "("):
        return False
    # check for variable identifier
    sc.scan()
    check, ident = identifier(sc)
    if(not check):
        return False
    if(sc.current() != ")"):
        return False
    sc.scan()
    if(not end_check(sc)):
        return False
    sc.next_line()
    #instruction_set.append("bne " + ident + " True " + str(end_line) )
    start_line = len(instruction_set)
    instruction_set.append("Loop Start")    #place holder for debug
    if(not block(sc)):
        return False
    instruction_set.append("jmp " + str(start_line + 1) )
    end_line = len(instruction_set) + 1    # Line after jump to start of loop
    instruction_set[start_line] = "bne " + ident + " True " + str(end_line)
    return True


def block(sc):
    token = sc.current()
    if(token != "{"):
        return False
    if(not end_check(sc)):
        return False
    # Enter a new scope within block
#    t.enterScope()
    sc.next_line()
    while(sc.current() != "}" ):
        if(not program_line(sc)):
            return False
        if(sc.next_line() == None):
            return False
    if(not end_check(sc)):
        return False
    # TODO: generate intermediate code based on returned values
#    instructions = t.exitScope()
    return True


def functionCall(sc):
    # TODO: check args and their type in function call is correct
    fn_name = sc.current()
    symbol_check = t.findSymbol(fn_name)
    # Check if the identifier is a function name in the symbol table
    if(symbol_check == False):
        return False
    # Check if identifier is has a type, otherwise it is a function
    if(symbol_check in cyd_types):
        return False
    sc.scan()
    count = 0
    if(sc.current() != "("):
        return False
    token = sc.scan()
    stack = []
    # TODO: type checking of args
    while(token != ")"):
        count += 1
        if( token == ","):
            count +=1
            token = sc.scan()   # Remove ','
        ident = sc.current()
        if (t.findSymbol(ident) == False):
            typeError(sc,"identifier not declared")
            return False
        token = sc.current()
        stack.append("push "+token)
        token = sc.scan()
    if(not end_check(sc)):
        return False
    info = t.findSymbol(fn_name)
    # check if number of args is correct
    if info[2] != count:
        return False

    #push input args
    for x in range (0,len(stack)):
        instruction_set.append(stack.pop())
    #generate jump instruction based on line number from symbol table
    instr = "jal "+str(info[0] + 2)
    instruction_set.append(instr)
    return True


def assignment(sc):
    check, ident = identifier(sc)
    if(check == False):
        return False
    if(ident in cyd_keywords):
         return False
    # check symbol table for identifier, throw error if not declared
    data_type = t.findSymbol(ident)
    if (data_type == False):
        typeError(sc,"identifier not declared")
        return False
    # Get variable type being assinged to
    
    if(sc.current() != "="):
        return False # Assignment statement needs an assignment
    sc.scan()
    # Check if variable is an int
    if data_type == "int":
        if functionCall(sc):
            instruction_set.append("pop " + ident) # get return value from stack and assign to variable
            return True
        check = expr(sc)
        if check:
            #get result of expression from stack
            instruction_set.append("pop " + ident)
            return True
    
    # Check if variable is a boolean
    elif data_type == "boolean":
        # Check if assigning a boolean literal
        check,value = boolean(sc)
        if check:
            instruction_set.append("load " + ident + " " + value)
            return True
        if functionCall(sc):
            instruction_set.append("pop " + ident) # get return value from stack and assign to variable
            return True
        check = logic(sc)
        if check:
            instruction_set.append("pop " + ident)
            return True
    return False


def declaration(sc):
    check, data_type = primative(sc)
    if(check == False):
        return False
    check, ident = identifier(sc)
    if(check == False):
        return False
    #Add identifier to symbol table
    if (t.enterSymbol(ident, data_type) != data_type):
        typeError(sc, "variable name has already been declared with a different type")
        return False
    # Check if declaration includes assignment
    if(not sc.empty()):
        if(sc.current() == "="):
            sc.scan()
                        
            # Check if variable is an int
            if data_type == "int":
                if functionCall(sc):
                    instruction_set.append("pop " + ident) # get return value from stack and assign to variable
                    return True
                check = expr(sc)
                if check:
                    #get result of expression from stack
                    instruction_set.append("pop " + ident)
                    return True
            
            # Check if variable is a boolean
            elif data_type == "boolean":
                # Check if assigning a boolean literal
                check,value = boolean(sc)
                if check:
                    instruction_set.append("load " + ident + " " + value)
                    return True
                if functionCall(sc):
                    instruction_set.append("pop " + ident) # get return value from stack and assign to variable
                    return True
                check = logic(sc)
                if check:
                    instruction_set.append("pop " + ident)
                    return True                        
            
    # This is true for declaration with no assignment, add to symbol table as 0
    instruction_set.append("load " + ident + " " + "0")
    return True
        


def expr(sc):
    check = term(sc)
    if not check:
        return False
    if not sc.empty():
        token = sc.current()
        if(token == "+"):
            sc.scan()
            check = expr(sc)
            if check:
                instruction_set.append("add")
                return True
            return False
        if(token == "-"):
            sc.scan()
            check = expr(sc)
            if check:
                instruction_set.append("sub")
                return True
            return False
    return True
    

        

def term(sc):
    check = factor(sc)
    if not check:
        return False
    if not sc.empty():
        token = sc.current()
        if(token == "*"):
            sc.scan()
            check = factor(sc)
            if check:
                instruction_set.append("mul")
                return True
            return False
        if(token == "/"):
            sc.scan()
            check = factor(sc)
            if check:
                instruction_set.append("div")
                return True
            return False
    return True
            
    
def factor(sc):
    token = sc.current()
    if token == "(":
        sc.scan()
        check = expr(sc)
        if(check and sc.current() == ")"):
            sc.scan()
            return True
        return False
    (check, token) = number(sc)
    if check:
        instruction_set.append("push " + token)
        return True
    (check, token) = identifier(sc)
    if check:
        instruction_set.append("push " + token)
        return True
    return False


"""
Looks for two arguments both can be either a number or an variable
creates corresponding instruction
"""
def logic(sc):
    # Get First Argument
    check, arg1 = number(sc)
    if not check:
        check, arg1 = identifier(sc)
        if not check:
            return False
            
    # Get Operator
    operator = sc.current()
    if operator not in logical_op:
        return False
    sc.scan()   # get next token after operator
    
    # Get Second Argument
    check, arg2 = number(sc)
    if not check:
        check, arg2 = identifier(sc)
        if not check:
            return False   
    instruction_set.append(opcode_dict[operator] + " " + arg1 + " " + arg2)
    return True



'''
============== Atomic Types =================
'''


def primative(sc):
    token = sc.current()
    if(token == "int" or token == "boolean"):
        sc.scan()
        return (True, token)
    #sc.disp_token_error("primative") This line causes error
    return (False, None)


def identifier(sc):
    token = sc.current()
    found = re.match("[a-zA-Z][a-zA-Z0-9_]*", token)
    if(found):
        if token in cyd_keywords:
            return (False, None)   # Identifier cannot be a keyword
        sc.scan()
        return (True, token)
    return (False, None)


def boolean(sc):
    token = sc.current()
    if(token == "True" or token == "False"):
        sc.scan()
        return (True, token)
    return (False, None)


def number(sc):
    token = sc.current()
    found = re.match("[0-9]+", token)
    if(found):
        sc.scan()
        return (True, token)
    return (False, None)


"""
Prints current token to program echo and returns True if the end of a line is reached
Otherwise throws an error and returns False
"""
def end_check(sc):
    #print sc.current(),
    if(not sc.empty()):
        sc.disp_token_error("end of this line")
        return False
    return True

"""
displays a type error message
"""
def typeError(sc, message):
    line = str(sc.get_line_number())
    print "  Type error at line " + line + ": " + message
    return



# Read program file
prog = program_file.read()

prog = prog.replace('\r', '')   # Removes '\r', making it Mac/Linux compatible
prog = prog.split('\n') # List of lines in program file

program_file.close()

# Run compiler
success = cydwinder_compiler(prog)
if success:
    print "  Program successfully compiled\n  Intermediate output file:\n    " + int_file

# Debug Parser output shows input code, parsed code and intermediate code generated
'''
# Display Program
print ("\nDisplaying Program:")
for x in prog:
    print x

# Display Parsed Output
print "\n\n========================================"
print "\nParser Output:"


# Display intermediate code generated
print "\n\n========================================"
print "\nIntermediate code:"
for x in instruction_set:
    print x
'''