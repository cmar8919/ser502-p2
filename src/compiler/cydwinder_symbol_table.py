class Symbol_Table(object):
    def __init__(self):
        super(Symbol_Table, self).__init__()
        self._stack = []
        self._stack.append({})
        self._activations = [[]]  #stores symbols in the order they were added to the local scope
        
        
    """
    Push table onto stack
    """
    def enterScope(self):
        self._stack.append({})
        self._activations.append([])


    """
    pop table off stack and return its contents
    """
    def exitScope(self):
        indentifiers = []
        i = len(self._stack)
        if i == 0:
            return indentifiers
        self._stack.pop() #pop local symbol table
        #changed to i-1 as [i] was throwing an index out of range error
        j = len(self._activations[i-1])
        for x in range(0, j):
            # Create list of pops in the oposite order they were pushed onto the stack
            # changed to i-1 as [i] was throwing an index out of range error
            indentifiers.append("pop " + self._activations[i-1].pop())
        self._activations.pop() #pop local activation record
        return indentifiers

    """
    add a symbol to the symbol table or return an entry symbol is in local scope

    datatype will int ,boolean, or line number (if it is a function)
    """
    def enterSymbol(self, ident, datatype):
        i = len(self._stack) - 1    
        if ident in self._stack[i]:         #check local scope for symbol match
            return self._stack[i][ident]    
        self._stack[i][ident] = datatype    #if symbol not in table, add entry
        self._activations[i].append(ident)  #add symbol to activation record
        return datatype #return variables own data type

    """
    look for symbol, if in table return its entry, otherwise return False
    """
    def findSymbol(self, ident):
        i = len(self._stack) - 1
        for x in range(i, -1, -1):
            if ident in self._stack[x]:
                return self._stack[x][ident]
        return False
        
        
    """
    look for symbol only in local scope, if in table return its entry, otherwise return False
    """
    def findLocalSymbol(self, ident):
        i = len(self._stack) - 1
        if (ident in self._stack[i]):
            return self._stack[x][ident]     # Sybmol is in local scope
        return False

    def display(self):
        print "\nSymbol Table (starting with global):"
        for x in range(0, len(self._stack)):
            print self._stack[x]
        print ""
        for x in self._activations:
            print x
