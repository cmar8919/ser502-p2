import sys

program_counter = 0
stack = []          # Python list used as a stack
call_stack = []
#symbol_table = {}   # Python dictionary used for the symbol table
symbol_table = [{}]

def instruction_decode(instruction):
    # Get first value
    value1 = find_symbol(instruction[1])
    if(value1 == None):
        value1 = literal_decode(instruction[1])
    # Get second value
    value2 = find_symbol(instruction[2])
    if(value2 == None):
        value2 = literal_decode(instruction[2])
    return (value1, value2)
    

def literal_decode(value):
    if value == "True":
        return True
    elif value == "False":
        return False
    return int(value)


"""
looks through all scopes starting with most local for symbol
"""
def find_symbol(sym):
    i = len(symbol_table) - 1
    for x in range(i, -1, -1):
        if sym in symbol_table[x]:
            return symbol_table[x][sym]
    return None
   
   
"""
adds/update symbol's value in most local for scope
"""  
def add_symbol(sym, value):
    i = len(symbol_table) - 1
    for x in range(i, -1, -1):
        if sym in symbol_table[x]:
            symbol_table[x][sym] = value
            return
    # Symbol not found in any scope, add to most local
    symbol_table[i][sym] = value
    return


'''
Get input file name to run as input argument
'''
if len(sys.argv) != 2:
    print "\n  ERROR: runtime expects a single file name\n"
    sys.exit()
    
file_name = sys.argv[1]

try: 
    program_file = open(file_name, 'r')
except IOError:
    print "\n  ERROR:  " + file_name + "  not in the local\n"
    sys.exit()


'''
Hard coded debug file names
'''
#file_name = "Test.cyd.int"
#file_name = "MathTest.cyd.int"
#file_name = "MathTest2.cyd.int"
#file_name = "LoopTest.cyd.int"
#file_name = "FunctionTest.cyd.int"
#file_name = "IfTest.cyd.int"
#file_name = "Fibonacci.cyd.int"
#file_name = "Factorial.cyd.int"


# Read intermediate language file
prog = program_file.read()

# Break up program into a list of lines
prog = prog.split('\n')
if(len(prog) == 0):
    line = "exit"
else:
    line = prog[program_counter]

# Run until an end is found
while(line != "exit"):
    x = line.split()
    # Check if line is empty
    if(len(x) == 0):
        print "Empty line"

    # 0 Argument Opcodes
    elif(len(x) == 1):
        # Addition
        if(x[0] == "add"):
            value1 = stack.pop()
            value2 = stack.pop()
            stack.append(value2 + value1)

        # Subtraction
        elif(x[0] == "sub"):
            value1 = stack.pop()
            value2 = stack.pop()
            stack.append(value2 - value1)

        # Multiplication
        elif(x[0] == "mul"):
            value1 = stack.pop()
            value2 = stack.pop()
            stack.append(value2 * value1)

        # Division
        elif(x[0] == "div"):
            value1 = stack.pop()
            value2 = stack.pop()
            stack.append(value2 / value1)



    # 1 Argument Opcodes
    elif(len(x) == 2):
        # Pop off of runtime stack
        if(x[0] == "pop"):
#            symbol_table[x[1]] = stack.pop()
            add_symbol(x[1], stack.pop())
            
        # puts variable in local symbol table
        elif(x[0] == "arg"):
           i = len(symbol_table) - 1
           symbol_table[i][x[1]] = stack.pop()
           

        # Push onto runtime stack
        elif(x[0] == "push"):
            value = find_symbol(x[1])
            if value != None:
                stack.append(value)
            elif(x[1] == "True"):
                stack.append(True)
            elif(x[1] == "False"):
                stack.append(False)
            else:
                stack.append( int(x[1]) )

        # Prompt for user input
        elif(x[0] == "get"):
            add_symbol(x[1], (input("Input: ")) )
#            symbol_table[x[1]] = (input("Input: "))

        # Display value to user
        elif(x[0] == "print"):
            print x[1] + ": " + str(find_symbol(x[1]))

        # Jump and link saves return address to call stack
        elif(x[0] == "jal"):
            call_stack.append(program_counter)
            symbol_table.append({})             # Add new scope symbol table
            program_counter = int(x[1]) - 2     # Update PC for jump

        # Return gets line number from call stack
        elif(x[0] == "return"):
            stack.append(find_symbol(x[1]))     # Put return value on stack
            symbol_table.pop()                  # Remove local function scope
            program_counter = call_stack.pop()  # Get return address
            
            
        elif(x[0] == "jmp"):
            program_counter = int(x[1]) - 2     # Update PC for jump
            

        # Can't find opcode
        else:
            print "No opcode match"


    # 2 Argument Opcodes
    elif(len(x) == 3):
        if(x[0] == "load"):
            add_symbol( x[1], literal_decode(x[2]) )

        # Set Less Than
        elif(x[0] == "slt"):
            (value1, value2) = instruction_decode(x)
            stack.append(value1 < value2)

        # Set Less Than or Equal to
        elif(x[0] == "slte"):
            (value1, value2) = instruction_decode(x)
            stack.append(value1 <= value2)

        # Set Greater Than
        elif(x[0] == "sgt"):
            (value1, value2) = instruction_decode(x)
            stack.append(value1 > value2)

        # Set Greater Than or Equal to
        elif(x[0] == "sgte"):
            (value1, value2) = instruction_decode(x)
            stack.append(value1 >= value2)

        # Equivalent
        elif(x[0] == "equ"):
            (value1, value2) = instruction_decode(x)
            stack.append(value1 == value2)

        # Equivalent
        elif(x[0] == "neq"):
            (value1, value2) = instruction_decode(x)
            stack.append(value1 != value2)
        


        # Can't find opcode
        else:
            print "No opcode match"

    # 3 Argument Opcodes
    # First argument is destination, second and third arguments are sources
    elif(len(x) == 4):
        

        # Branch if not equal
        if(x[0] == "bne"):
            # Get 2 values to be compared
            (value1, value2) = instruction_decode(x)            
            # Compare values for branch
            if(value1 != value2):
                program_counter = int(x[3]) - 2    # Update PC for branch
            
        
        # Branch if equal
        elif(x[0] == "beq"):
            # Get 2 values to be compared
            (value1, value2) = instruction_decode(x)            
            # Compare values for branch
            if(value1 == value2):
                program_counter = int(x[3]) - 2    # Update PC for branch            
            

        # Can't find opcode
        else:
            print "No opcode match"
    
    # Can't find opcode
    else:
        print "Instruction has too many elements"
        
    
    # Update PC
    program_counter += 1
    
    # Check if PC is in program
    if(program_counter >= len(prog)):
        print "Last line in program found without an end"
        line = "exit"
    # Fetch new instruction
    line = prog[program_counter]

program_file.close()