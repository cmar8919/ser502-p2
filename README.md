# SER 502 Project 2 #

## Team 4:
- Christopher Mar
- Akshat Kale
- Vignesh Kannan
- Aparna Padki

### Build Information
Both the compiler (cydc.py) and runtime (clither.py) are written using Python 2.7.  No building is
required, but the following Python files must be in the compiler directory:

- cydwinder_scanner.py
- cydwinder_symbol_table.py

###Directory Information
**data:**
example source code


**doc:**
documentation (presentation and grammar)

**src:**
compiler directory and runtime directory


###Compiler:
	python cydc.py <file_name>.cyd
  
**generates intermidiate file:** *<file_name>*.cyd.int
  
###Runtime:
	python clither.py <file_name>.cyd.int
